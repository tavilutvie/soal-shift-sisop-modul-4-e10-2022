#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>

static const char *dirPath = "/home/milkyway/Documents";
static const char *logPath = "/home/milkyway/Wibu.log";

char *awalan1 = "Animeku_";
int x = 0;

void writeTheLog(char *nama, const char *from, const char *to)
{
	time_t rawtime;
	time(&rawtime);

	char textLog[1000];

	FILE *file;
	file = fopen(logPath, "a");

	if (strstr(to, awalan1)) sprintf(textLog, "RENAME\tterenkripsi\t%s\t-->\t%s\n", from, to);
	else sprintf(textLog, "RENAME\tterdecode\t%s\t-->\t%s\n", from, to);

	fputs(textLog, file);
	fclose(file);
	return;
}

int indexTanpaExt(char *path)
{
	int flag = 0;
	for (int i = strlen(path) - 1; i >= 0; i--)
	{
		if (path[i] == '.')
		{
			if (flag == 1)
				return i;
			else
				flag = 1;
		}
	}
	return strlen(path);
}

int indexSetelahSlash(char *path, int hasil)
{
	for (int i = 0; i < strlen(path); i++)
	{
		if (path[i] == '/')
			return i + 1;
	}
	return hasil;
}

void enkripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, 0);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}

void dekripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, idAkhir);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
	int result;
	char filepath[1000];

	char *a = strstr(path, awalan1);
	if (a != NULL)
		dekripsiAwalan1(a);

	if (strcmp(path, "/") == 0)
	{
		path = dirPath;
		sprintf(filepath, "%s", path);
	}
	else
	{
		sprintf(filepath, "%s%s", dirPath, path);
	}

	result = lstat(filepath, stbuf);
	if (result == -1)
		return -errno;
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	char filepath[1000];

	char *a = strstr(path, awalan1);
	if (a != NULL)
		dekripsiAwalan1(a);

	if (strcmp(path, "/") == 0)
	{
		path = dirPath;
		sprintf(filepath, "%s", path);
	}
	else
	{
		sprintf(filepath, "%s%s", dirPath, path);
	}

	int result = 0;
	DIR *dp;
	struct dirent *de;

	(void)offset;
	(void)fi;

	dp = opendir(filepath);
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL)
	{
		if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
			continue;

		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (a != NULL)
			enkripsiAwalan1(de->d_name);

		result = (filler(buf, de->d_name, &st, 0));
		if (result != 0)
			break;
	}

	closedir(dp);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	char filepath[1000];

	char *a = strstr(path, awalan1);
	if (a != NULL)
		dekripsiAwalan1(a);

	if (strcmp(path, "/") == 0)
	{
		path = dirPath;
		sprintf(filepath, "%s", path);
	}
	else
	{
		sprintf(filepath, "%s%s", dirPath, path);
	}

	int result = 0;
	int fd = 0;

	(void)fi;

	fd = open(filepath, O_RDONLY);
	if (fd == -1)
		return -errno;

	result = pread(fd, buf, size, offset);
	if (result == -1)
		result = -errno;

	close(fd);
	return result;
}

static int xmp_rename(const char *from, const char *to){
	int result;
	char oldDir[1000], newDir[1000];
	
	char *a = strstr(to, awalan1);
	if (a != NULL) dekripsiAwalan1(a);

	sprintf(oldDir, "%s%s", dirPath, from);
	sprintf(newDir, "%s%s", dirPath, to);

	result = rename(oldDir, newDir);
	if (result == -1) return -errno;

	writeTheLog("RENAME", oldDir, newDir);

	return 0;
}

static struct fuse_operations xmp_oper = {
	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.rename = xmp_rename,
};

int main(int argc, char *argv[])
{
	umask(0);
	return fuse_main(argc, argv, &xmp_oper, NULL);
}