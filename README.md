# SISOP Modul 4 (Kelompok E10)
**soal-shift-sisop-modul-4-E10-2022**

## Anggota Kelompok
No  | Nama                      |NRP
--- | ------------------------- | ------------
1   | Hafiz Kurniawan           | (5025201032)
2   | Eldenabih Tavirazin Lutvie| (5025201213)
3   | Michael Ariel Manihuruk   | (5025201158)

## Soal 1
<br>
1.	Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
<br>
<br>
(a.)   Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13.

Contoh : 
	"Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt"
<br>
<br>
(b.)   Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan   1a.
<br>
<br>
(c.)   Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
<br>
<br>
(d.)   Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
<br>
<br>
(e.)	Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
<br>

## Pengerjaan
Terdapat beberapa fungsi yang dibuat untuk membantu dalam melakukan pembacaan direktori. pisahkanExt berfungsi untuk mengambil index tanpa ekstensi. Sedangkan fungsi indexSetelahSlash digunakan untuk mengambil indeks setelah slash. Jika tidak ditemukan slash pada path yang sedang di cek, maka akan dikembalikan nilai parameter 'hasil' yang dimasukkan. <br>
```c
int indexTanpaExt(char *path)
{
	int flag = 0;
	for (int i = strlen(path) - 1; i >= 0; i--)
	{
		if (path[i] == '.')
		{
			if (flag == 1)
				return i;
			else
				flag = 1;
		}
	}
	return strlen(path);
}

int indexSetelahSlash(char *path, int hasil)
{
	for (int i = 0; i < strlen(path); i++)
	{
		if (path[i] == '/')
			return i + 1;
	}
	return hasil;
}
```
Untuk melakukan enkripsi pada path, digunakan fungsi enkripsiAwalan1. Fungsi ini akan mengambil indeks awal dan akhir dari bagian path yang ingin di enkripsi. Setelah mendapat indeks awal dan akhir untuk melakukan enkripsi, maka selanjutnya dilakukan enkripsi. Program akan melakukan iterasi dari indeks awal hingga akhir. Ketika ditemukan huruf besar, maka akan dilakukan enkripsi atbash dengan mengurangkan karakter tersebut dengan 'A' kemudian akan dilakukan pengurangan (25 - karakter tersebut). Sedangkan jika ditemukan huruf kecil, maka akan dilakukan enkripsi rot13 dengan menambahkan karakter tersebut dengan 13 setelah karakter tersebut dikurang dengan karakter 'a'. Setelah itu di mod 26. Setelah dilakukan pengurangan untuk atbash dan penambahan untuk rot13, maka akan dilakukan penambahan kembali karakter awal.
```c
void enkripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, 0);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}
```
Kemudian untuk dekripsi, tidak berbeda jauh dengan enkripsi sebelumnya karena algoritma yang digunakan adalah atbash dan rot13. <br>
```c
void dekripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, idAkhir);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}
```
Untuk melakukan enkripsi ketika mendapatkan awalan 'Animeku_', pada fungsi readdir FUSE, ditambahkan code berikut: <br>
```c
char *a = strstr(path, awalan1);
	if (a != NULL)
		dekripsiAwalan1(a);
```
dengan isi variabel awalan1 adalah sebagai berikut:
```c
static const char *dirPath = "/home/milkyway/Documents";
```
Untuk membuat log, dibuat fungsi writeTheLog sebagai berikut. Fungsi tersebut bekerja dengan cara mengambil waktu, setelah itu memasukkannya ke file dengan path textLog. Ketika pada nama akhir path tersebut ditemukan text yang ada pada awalan1, maka informasi yang ditulis adalah enkripsi. Jika tidak, maka informasi yang ditulis adalah decode.
```c
void writeTheLog(char *nama, const char *from, const char *to)
{
	time_t rawtime;
	time(&rawtime);

	char textLog[1000];

	FILE *file;
	file = fopen(logPath, "a");

	if (strstr(to, awalan1)) sprintf(textLog, "RENAME\tterenkripsi\t%s\t-->\t%s\n", from, to);
	else sprintf(textLog, "RENAME\tterdecode\t%s\t-->\t%s\n", from, to);

	fputs(textLog, file);
	fclose(file);
	return;
}
```
Karena pembuatan log hanya ada pada saat melakukan fungsi rename, maka fungsi tersebut akan dipanggil di fungsi rename pada FUSE.


### Screenshot

![image](soal1/gambar1.jpeg) <br>

![image](soal1/gambar2.jpeg) <br>

![image](soal1/gambar3.jpeg) <br>

![image](soal1/gambar4.jpeg) <br>

![image](soal1/gambar5.jpeg) <br>

![image](soal1/gambar6.jpeg) <br>


### Kendala
Lebaran gabawa laptop :(
